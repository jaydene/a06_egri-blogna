document.write("<table><tr><th colspan='3'> Numbers 5 to 15 with Squares and Cubes </th></tr>");
document.write("<tr><td>Number</td><td>Square</td><td>Cube</td></tr>");
for(var n=5; n<=15; n++) {
    document.write("<tr><td>" + n + "</td><td>" + Math.pow(n, 2) + "</td><td>" + Math.pow(n,3) + "</td></tr>");
}
document.write("</table>");
